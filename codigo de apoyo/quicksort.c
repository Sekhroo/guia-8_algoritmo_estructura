#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define LIMITE_SUPERIOR 5000
#define LIMITE_INFERIOR 1
#define TRUE 0
#define FALSE 1

void imprimir_vector(int a[], int n) {
  int i;

  for (i=0; i<n; i++) {
    printf ("a[%d]=%d ", i, a[i]);
  }
  printf("\n\n");
}

void ordena_quicksort(int a[], int n) {
  int tope, ini, fin, pos;
  int pilamenor[100];
  int pilamayor[100];
  int izq, der, aux, band;
  
  tope = 0;
  pilamenor[tope] = 0;
  pilamayor[tope] = n-1;
  
  while (tope >= 0) {
    ini = pilamenor[tope];
    fin = pilamayor[tope];
    tope = tope - 1;
    
    // reduce
    izq = ini;
    der = fin;
    pos = ini;
    band = TRUE;
    
    while (band == TRUE) {
      while ((a[pos] <= a[der]) && (pos != der))
        der = der - 1;
        
      if (pos == der) {
        band = FALSE;
      } else {
        aux = a[pos];
        a[pos] = a[der];
        a[der] = aux;
        pos = der;
        
        while ((a[pos] >= a[izq]) && (pos != izq))
          izq = izq + 1;
          
        if (pos == izq) {
          band = FALSE;
        } else {
          aux = a[pos];
          a[pos] = a[izq];
          a[izq] = aux;
          pos = izq;
        }
      }
    }
    
    if (ini < (pos - 1)) {
      tope = tope + 1;
      pilamenor[tope] = ini;
      pilamayor[tope] = pos - 1;
    }
    
    if (fin > (pos + 1)) {
      tope = tope + 1;
      pilamenor[tope] = pos + 1;
      pilamayor[tope] = fin;
    }
  }
}

/* principal */
int main(int argc, char *argv[]) {
  int n = atoi(argv[1]);
  int a[n];
  int i;
  int elemento;

  clock_t t1, t2;
  double secs;
    
  // llena el vector.
  srand (time(NULL));
  for (i=0; i<n; i++) {
    elemento = (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
    a[i] = elemento;
  }

  imprimir_vector(a, n);
  
  // llama al método de ordenamiento.
  t1 = clock();
  ordena_quicksort(a, n);
  t2 = clock();

  imprimir_vector(a, n);

  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
  printf("%.0f milisegundos\n",secs * 1000.0);

  return 0;
}