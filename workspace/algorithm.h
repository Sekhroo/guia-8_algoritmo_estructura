#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <iostream>
using namespace std;

class algorithm {

    private:

    public:
        algorithm();

        // Función para mostrar datos de un vector en consola
        void mostrar_vector(int vector[], int tamano_vector, string verificar_visual);

        // Funciones para ordenar un vector mediante el algoritmo de quicksort
        void algorithm_quicksort(int vector[], int tamano_vector, string verificar_visual);
        void quicksort(int vector[], int tamano_vector);

        // Funciones para ordenar un vector mediante el algoritmo selection
        void algorithm_selection(int vector[], int tamano_vector, string verificar_visual);
        void selection(int vector[], int tamano_vector);


};
#endif