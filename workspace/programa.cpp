#include <cstring>
#include <cstdlib>
#include <ctime>
#include <iostream>

#define LIMITE_SUPERIOR 9999
#define LIMITE_INFERIOR 1

using namespace std;

#include "algorithm.h"


/* Función main */
int main(int argc, char **argv){

    // Valida cantidad de parámetros mínimos.
    if (argc<3) {
		cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma: " << endl;
		cout << "1.-Ejemplo: ./programa 10 s" << endl;
        cout << "2.-Ejemplo: ./programa 10234 n" << endl;

        return 0;
    }

    // Lectura de los parametros de entrada
    int tamano_vector = atoi(argv[1]);
    string verificar_visual = argv[2];

    // Verifica el parámetro de visualización.
    if (verificar_visual != "s" && verificar_visual != "S" && verificar_visual != "n" && verificar_visual != "N"){
        cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa lee un caracter (s/n) para mostrar o no los vectores completos: " << endl;
		cout << "1.-Ejemplo: ./programa 10 s" << endl;
        cout << "2.-Ejemplo: ./programa 10234 n" << endl;

        return 0;
    }

    // Instanciar metodos de los algoritmos
    algorithm *algorithm_ = new algorithm();

    // Crear el vector con el tamaño según los parametros de entrada
    int vector[tamano_vector];

    // Rellenar el vector de tamaño N con números aleatorios del 9999 al 1
    srand((unsigned) time(0));
    for (int i = 0; i < tamano_vector; i++) {
        vector[i] =  (rand() % LIMITE_SUPERIOR) + LIMITE_INFERIOR;
    }

    // Mostrar vector desordenado con números aleatorios
    algorithm_->mostrar_vector(vector,tamano_vector, verificar_visual);

    // Ordenar el vector desordenado con el algoritmo de selection y medir el tiempo que tarda
    algorithm_->algorithm_selection(vector,tamano_vector, verificar_visual);

    // Ordenar el vector desordenado con el algoritmo de quicksort y medir el tiempo que tarda
    algorithm_->algorithm_quicksort(vector,tamano_vector, verificar_visual);

    return 0;
}