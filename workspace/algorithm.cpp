#include <cstring>
#include <cstdlib>
#include <ctime>
#include <iostream>

#define TRUE 0
#define FALSE 1

using namespace std;

#include "algorithm.h"

algorithm::algorithm(){}

// Función para mostrar los datos almacenados en el vector
void algorithm::mostrar_vector(int vector[], int tamano_vector, string verificar_visual) {

    // Verficiar si el usuario quiere ver los datos
    if (verificar_visual == "n" || verificar_visual == "N"){
        return;

    // En el caso de que si, mostrarlos
    }else if (verificar_visual == "s" || verificar_visual == "S") {
        cout << "Vector: ";

        // Bucle para imprimirlos en la terminal
        for (int i=0; i<tamano_vector; i++) {
            cout << vector[i] << " ";
        }

        cout << endl << endl;
    }
}

// Función para iniciar el algoritmo de quicksort
void algorithm::algorithm_quicksort(int vector[], int tamano_vector, string verificar_visual) {

    cout << "=========================================================" << endl;
    cout << "== . . . . . . . Algoritmo Quicksort . . . . . . . . . ==" << endl;
    cout << "=========================================================" << endl;
    
    // Crear un vector nuevo para copiar el original
    int vector_quicksort[tamano_vector];

    // Se iguala al original
    for (int i=0; i < tamano_vector; i++) {
        vector_quicksort[i] = vector[i];
    }

    // Variables para medir el tiempo de ejecución
    clock_t t1, t2;
    double secs;

    // Comienzar el registro del tiempo
    t1 = clock();

    // Algoritmo quicksort para ordenar
    quicksort(vector_quicksort, tamano_vector);
    
    // Acaba el registro del tiempo
    t2 = clock();

    // Se muestra el vector resultante del reordenamiento quicksort
    mostrar_vector(vector_quicksort, tamano_vector, verificar_visual);

    // Se calcula el tiempo demorado y se muestra
    secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
    cout << "Tiempo de ejecución: " << secs * 1000.0 << " milisegundos" << endl;
    cout << "=========================================================" << endl;
    cout << "=========================================================" << endl << endl;
}

// Función para efectuar el algoritmo de quicksort en si
void algorithm::quicksort(int vector[], int tamano_vector) {
    
    // Inicializa las variables y vectores
    int tope, ini, fin, pos;
    int pilamenor[100];
    int pilamayor[100];
    int izq, der, aux, band;
    
    // Aborda el problema con pilas, asigna los valores pertinentes (tamaño N)
    tope = 0;
    pilamenor[tope] = 0;
    pilamayor[tope] = tamano_vector-1;
    
    // Ciclo hasta acabar con el tope menor a 0
    while (tope >= 0) {

        // Inicia los valores
        ini = pilamenor[tope];
        fin = pilamayor[tope];

        // Va reduciendo el tope en 1
        tope = tope - 1;
    
        // Reduce
        izq = ini;
        der = fin;
        pos = ini;
        band = TRUE;

        // Realiza los cambios pertinentes para ordenar el vector evaluando las variables
        while (band == TRUE) {
            while ((vector[pos] <= vector[der]) && (pos != der))
                der = der - 1;
        
            if (pos == der) {
                band = FALSE;
            } else {
                aux = vector[pos];
                vector[pos] = vector[der];
                vector[der] = aux;
                pos = der;
        
                while ((vector[pos] >= vector[izq]) && (pos != izq))
                    izq = izq + 1;
          
                if (pos == izq) {
                    band = FALSE;
                } else {
                    aux = vector[pos];
                    vector[pos] = vector[izq];
                    vector[izq] = aux;
                    pos = izq;
                }
            }
        }

        // Revalida las pilas
        if (ini < (pos - 1)) {
            tope = tope + 1;
            pilamenor[tope] = ini;
            pilamayor[tope] = pos - 1;
        }

        if (fin > (pos + 1)) {
            tope = tope + 1;
            pilamenor[tope] = pos + 1;
            pilamayor[tope] = fin;
        }
    }
}

// Función para iniciar el algoritmo de Selection
void algorithm::algorithm_selection(int vector[], int tamano_vector, string verificar_visual) {
    cout << "=========================================================" << endl;
    cout << "== . . . . . . . Algoritmo Selection . . . . . . . . . ==" << endl;
    cout << "=========================================================" << endl;
    
    // Crear un vector nuevo para copiar el original
    int vector_selection[tamano_vector];

    // Se iguala al original
    for (int i=0; i < tamano_vector; i++) {
        vector_selection[i] = vector[i];
    }

    // Variables para medir el tiempo de ejecución
    clock_t t1, t2;
    double secs;

    // Comienzar el registro del tiempo
    t1 = clock();

    // Algoritmo selection para ordenar
    selection(vector_selection, tamano_vector);
    
    // Acaba el registro del tiempo
    t2 = clock();

    // Se muestra el vector resultante del reordenamiento selection
    mostrar_vector(vector_selection, tamano_vector, verificar_visual);

    // Se calcula el tiempo demorado y se muestra
    secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
    cout << "Tiempo de ejecución: " << secs * 1000.0 << " milisegundos" << endl;
    cout << "=========================================================" << endl;
    cout << "=========================================================" << endl << endl;
}

// Función para efectuar el algoritmo de selection en si
void algorithm::selection(int vector[], int tamano_vector) {
    
    // Inicializar variables para asistir en el ordenamiento
    int menor, k;
    
    // Bucle para evaluar el primer dato
    for (int i=0; i<=tamano_vector-2; i++) {
        menor = vector[i];
        k = i;

        // Bucle para buscar un dato menor
        for (int j=i+1; j<=tamano_vector-1; j++) {
            if (vector[j] < menor) {
                menor = vector[j];
                k = j;
            }
        }

        // Asignar
        vector[k] = vector[i];
        vector[i] = menor;
    }
}
